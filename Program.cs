﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace reflection
{
    class Program
    {
        private static BaseSharp _inCeptor;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // var porsche = new Car
            // {
            //     Id = 1,
            //     Name = "Porsche Cayon",
            //     Birthday = DateTime.Now,
            //     IsDelete = false
            // };

            // Type typeCar = typeof(Car);

            // var properties = typeCar.GetProperties();
            // var fields = typeCar.GetFields();

            // var property = typeCar.GetProperty("Name");
            // property.SetValue(porsche, "BMW M6", null);

            // var field = typeCar.GetField("Dino");
            // field.SetValue(porsche, "System Test");

            _inCeptor = new BaseSharp();
            _inCeptor.DrawImage();

            string textFile = @"
                    using System;

                    namespace reflection
                    {
                        public class BaseSharp
                        {
                            public virtual void DrawImage()
                            {
                                {{action}};
                            }
                        }
                    }
                    ";

            string action = "Console.WriteLine(\"BaseSharp main program Reflection\")";
            textFile = textFile.Replace("{{action}}", action);

            IReadOnlyCollection<MetadataReference> _references = new[] {
                  MetadataReference.CreateFromFile(typeof(Binder).GetTypeInfo().Assembly.Location),
                  MetadataReference.CreateFromFile(typeof(ValueTuple<>).GetTypeInfo().Assembly.Location)
                 };

            List<string> lstName = new List<string>();

            lstName.Add("System");
            lstName.Add("System.Runtime");
            lstName.Add("netstandard");
            lstName.Add("System.ComponentModel");

            var metadataReferLib = lstName.Select(s => MetadataReference.CreateFromFile(Assembly.Load(s).Location) as MetadataReference);

            var options = new CSharpParseOptions(kind: SourceCodeKind.Regular, languageVersion: LanguageVersion.CSharp8);
            SyntaxTree mySyntaxTree = CSharpSyntaxTree.ParseText(textFile, options);

            var compilationOptions = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary,
                            optimizationLevel: OptimizationLevel.Release,
                            allowUnsafe: true);
            Compilation compilation = CSharpCompilation.Create("DynamicFile", options: compilationOptions, references: _references)
                                                        .AddReferences(metadataReferLib)
                                                        .AddSyntaxTrees(mySyntaxTree);

            var stream = new MemoryStream();
            var pdbStream = new MemoryStream();

            var emitResult = compilation.Emit(stream, pdbStream);

            Assembly dynamicAsFile = Assembly.Load(stream.ToArray(), pdbStream.ToArray());

            var typeAs = dynamicAsFile.GetTypes().FirstOrDefault(t => typeof(BaseSharp).IsAssignableFrom(t));
            var instance = Activator.CreateInstance(typeAs) as BaseSharp;

            instance.DrawImage();
        }
    }

    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public bool IsDelete { get; set; }
        public string Dino;
        public Delegate HandlerMachine;
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
